.PHONY: all install pi test

all:
	cargo build
install: all pi
	cp ./target/debug/t ~/bin/
	scp ./target/armv7-unknown-linux-gnueabihf/debug/t pi:~/bin
pi:
	cargo build --target armv7-unknown-linux-gnueabihf
test:
	cargo test
