x 1970-01 start an era
x 2021-01-20 Протестировать совместимость с кириллицей
x 2021-01-20 Upload the app to bitbucket
x 2021-01-20 Move this app to the home server
x 2021-01-20 Move all functions to lib
x 2021-01-23 Wire the actual action
x 2021-01-23 Refactor add to return Result instead of panicing
x 2021-01-23 Refactor commands to use Option<String> for dates
2021-01-23 0 Allow rearranging parameters
2021-01-25 0 Show unfinished tasks from the past when current is requested
2021-02 0 Colorize the output
2021-02 1 Color dates in the past red
2021-02 2 Add an integrity check for tasks
2021-02 3 Print weekdays with dates
2021-02 4 Add variation to congrats text
2021-02 5 Add more human-writable day identificators, such as 'tomorrow', 'next Friday', etc.
2021-02 6 Write a telegram bot interface
2021-02 7 Add README.md
2021-02-07 0 third
2021-02-07 1 second
2021-02-07 2 first
x 2021-03 Convert this program to a library
2021-03 0 Refactor this rusty code
