use std::env;

use todo;
use todo::{read_tasks, write_tasks, types::*};

use clap::{App, Arg, SubCommand};

const TODO_FILE: &str = "todo.md";

fn main() {
    let file_help: &str = &format!("use a custom todo-file; defaults to '{}'", TODO_FILE)[..];
    let matches = App::new("t")
        .version("0.1.0")
        .author("Igor Kislitsyn <k.yngvarr@gmail.com>")
        .about("Gets your tasks done!")
        .arg(Arg::with_name("file")
            .short("f")
            .long("file")
            .takes_value(true)
            .help(&file_help))
        .subcommand(SubCommand::with_name("add")
            .about("add a task to the list")
            .arg(Arg::with_name("top")
                .short("t")
                .long("top")
                .help("add with the highest priority"))
            .arg(Arg::with_name("due")
                .short("d")
                .long("at")
                .takes_value(true)
                .help("add to a specific day; defaults to today"))
            .arg(Arg::with_name("done")
                .long("done")
                .help("mark as done"))
            .arg(Arg::with_name("text")
                .required(true)
                .help("task text, supports #tags")))
        .subcommand(SubCommand::with_name("rm")
            .about("remove task")
            .arg(Arg::with_name("priority")
                .help("priority of a task to be deleted; defaults to current"))
            .arg(Arg::with_name("due")
                .short("d")
                .long("at")
                .takes_value(true)
                .help("due date for a task; defaults to today")))
        .subcommand(SubCommand::with_name("done")
            .about("mark a task as done")
            .arg(Arg::with_name("priority")
                .help("priority of a task to be marked; defaults to current"))
            .arg(Arg::with_name("due")
                .short("d")
                .long("at")
                .takes_value(true)
                .help("due date for a task; defaults to today")))
        .subcommand(SubCommand::with_name("mv")
            .about("move a task to the different date")
            .arg(Arg::with_name("priority")
                .help("priority of a task to be moved; defaults to current"))
            .arg(Arg::with_name("to")
                .long("to")
                .takes_value(true)
                .help("a date to move the task to; defaults to today"))
            .arg(Arg::with_name("from")
                .long("from")
                .takes_value(true)
                .help("due date for a task; defaults to today"))
            .arg(Arg::with_name("at")
                .long("at")
                .takes_value(true)
                .help("combines --to and --from"))
            .arg(Arg::with_name("top")
                .short("t")
                .long("top")
                .help("add with the highest priority")))
        .subcommand(SubCommand::with_name("swap")
            .about("swap priorities of commands")
            .arg(Arg::with_name("task1")
                .required(true)
                .help("the first task's priority"))
            .arg(Arg::with_name("task2")
                .required(true)
                .help("the second task's priority"))
            .arg(Arg::with_name("at")
                .long("at")
                .takes_value(true)
                .help("a date to swap tasks at; defaults to today")))
        .subcommand(SubCommand::with_name("list")
            .about("show unfinished tasks for today")
            .arg(Arg::with_name("at")
                .long("at")
                .takes_value(true)
                .help("show only the specified day; defaults to today"))
            .arg(Arg::with_name("done")
                .short("d")
                .long("done")
                .help("include tasks that are already done"))
            .arg(Arg::with_name("all")
                .short("a")
                .long("all")
                .help("show all the planned tasks")))
        .get_matches();

    let todo_file = env::var("TODO_FILE").unwrap_or(String::from(TODO_FILE));
    let todo_file = matches.value_of("file").unwrap_or(&todo_file[..]);
    let mut tasks = read_tasks(todo_file);

    if let Some(matches) = matches.subcommand_matches("add") {
        let result = todo::add(&mut tasks,
            matches.value_of("due").map(String::from),
            matches.is_present("done"),
            String::from(matches.value_of("text").unwrap()),
            matches.is_present("top"));

        if let Err(err) = result {
            eprintln!("Error: {}", err);
            return;
        }

        write_tasks(todo_file, &mut tasks);
        return;
    }

    if let Some(matches) = matches.subcommand_matches("rm") {
        let result = todo::rm(&mut tasks,
            matches.value_of("due").map(String::from),
            matches.value_of("priority").map(String::from));

        if let Err(err) = result {
            eprintln!("Error: {}", err);
            return;
        }

        write_tasks(todo_file, &mut tasks);
        return;
    }

    if let Some(matches) = matches.subcommand_matches("done") {
        let result = todo::done(&mut tasks,
            matches.value_of("due").map(String::from),
            matches.value_of("priority").map(String::from));

        if let Err(err) = result {
            eprintln!("Error: {}", err);
            return;
        }

        write_tasks(todo_file, &mut tasks);
        todo::show_current(&tasks);
        return;
    }

    if let Some(matches) = matches.subcommand_matches("mv") {
        let result = todo::mv(&mut tasks,
            matches.value_of("from").map(String::from),
            matches.value_of("to").map(String::from),
            matches.value_of("at"),
            matches.value_of("priority").map(String::from),
            matches.is_present("top"));

        if let Err(err) = result {
            eprintln!("Error: {}", err);
            return;
        }

        write_tasks(todo_file, &mut tasks);
        return;
    }

    if let Some(matches) = matches.subcommand_matches("swap") {
        let at = matches.value_of("at");
        let result = todo::swap(&mut tasks,
            matches.value_of("task1").map(String::from),
            matches.value_of("task2").map(String::from),
            at.map(String::from));

        if let Err(err) = result {
            eprintln!("Error: {}", err);
            return;
        }

        write_tasks(todo_file, &mut tasks);
        todo::print_list(&tasks, at.map(String::from), false, false);
        return;
    }

    if let Some(matches) = matches.subcommand_matches("list") {
        let result = todo::print_list(&tasks,
            matches.value_of("at").map(String::from),
            matches.is_present("done"),
            matches.is_present("all"));

        if let Err(err) = result {
            eprintln!("Error: {}", err);
            return;
        }
        return;
    }

    let today = Date::today();
    let debt: Vec<&Task> = tasks.iter().filter(|x| x.due < today && !x.done).collect();
    if debt.len() > 0 {
        println!("You've got something undone in the past. Please, do some planning.");
        todo::print_list(&tasks, None, false, true);
    } else {
        todo::show_current(&tasks);
    }
}
