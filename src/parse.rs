use crate::types::Date;

#[derive(Debug)]
pub enum Command {
    // due, text, top, done
    Add(Option<Date>, String, bool, bool),
    Done
}

fn is_quote(ch: char) -> bool {
    ch == '"' || ch == '\'' || ch == '`'
}

pub fn tokenize(msg: &String) -> Vec<String>{
    let mut tokens: Vec<String> = vec![];
    let mut token = String::from("");
    let mut quoted: Option<char> = None;

    let mut prev = 'a';
    let mut drop: bool;
    let msg = msg.trim();

    for ch in msg.chars() {
        drop = false;

        if !drop && prev == ' ' && ch == ' ' { continue }

        // no multiple spaces
        if !drop && prev != ' ' && ch == ' ' {
            if quoted == None {
                tokens.push(token);
                token = String::from("");
            } else {
                token.push(ch);
            }
            drop = true;
        }

        // ch != ' '
        if is_quote(ch) {
            if quoted == Some(ch) {
                tokens.push(token);
                token = String::from("");
            } else if quoted == None {
                quoted = Some(ch);
            } else {
                token.push(ch);
            }
            drop = true;
        }

        // ch != ' ' && !is_quote(ch)
        if !drop {
            token.push(ch);
        }

        prev = ch;
    }

    if token.len() != 0 {
        tokens.push(token);
    }

    tokens
}

pub fn parse_str(msg: &str) -> Option<Command> {
    parse(&String::from(msg))
}

pub fn parse(msg: &String) -> Option<Command> {
    let ts = tokenize(msg);
    parse_cmd(ts)
}

fn parse_cmd(ts: Vec<String>) -> Option<Command> {
    if ts.len() == 0 { return None; }

    if ts[0] == "добавь" || ts[0] == "add" {
        cmd_add(&ts[1..])
    } else if ts[0] == "done" {
        Some(Command::Done)
    } else {
        None
    }
}

// TODO allow rearranging parameters
fn cmd_add(ts: &[String]) -> Option<Command> {
    if ts.len() == 0 { return None; }
    let (due, ts) = to_date(ts);
    let (top, ts) = to_top(ts);
    let (done, ts) = is_done(ts);
    if ts.len() == 0 { return None; }
    let text = ts.join(" ");
    Some(Command::Add(due, text, top, done))
}

fn to_date(ts: &[String]) -> (Option<Date>, &[String]) {
    if ts.len() < 2 { return (None, ts); }
    if ts[0] != "на" { return (None, ts); }
    match &ts[1][..] {
        "сегодня" => (Date::from("today"), &ts[2..]),
        _ => (Date::from(&ts[1][..]), &ts[2..])
    }
}

fn to_top(ts: &[String]) -> (bool, &[String]) {
    if ts.len() < 2 { return (false, ts); }
    if (&ts[0][..], &ts[1][..]) == ("в", "начало") { (true, &ts[2..]) }
    else { (false, ts) }
}

fn is_done(ts: &[String]) -> (bool, &[String]) {
    if ts.len() < 1 { return (false, ts); }
    if &ts[0][..] == "сделанное" { (true, &ts[1..]) }
    else { (false, ts) }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn add_parsing() {
        let due = Date::from("2021-01-25");
        let text = String::from("поздравить папу");

        assert_eq!(parse_str("добавь на 2021-01-25 'поздравить папу'"), Command::Add(due, text, false, false));
        assert_eq!(parse_str("добавь на 2021-01-25 в начало 'поздравить папу'"), Command::Add(due, text, true, false));
        assert_eq!(parse_str("добавь на 2021-01-25 сделанное 'поздравить папу'"), Command::Add(due, text, false, true));
        assert_eq!(parse_str("добавь на 2021-01-25 в начало сделанное 'поздравить папу'"), Command::Add(due, text, true, true));
    }
}
