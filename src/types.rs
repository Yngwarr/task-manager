use std::fmt;
use std::cmp::Ordering;
use regex::Regex;
use chrono::prelude::{Local, Datelike, Weekday, TimeZone};
use chrono::offset::LocalResult;

#[derive(Eq, Copy, Clone, Debug)]
pub enum Date {
    // YYYY-MM-DD
    Day(i32, u32, u32),
    // YYYY-MM
    Month(i32, u32)
}

impl Date {
    pub fn today() -> Date {
        let now = Local::today();
        Date::Day(now.year(), now.month(), now.day())
    }

    pub fn tomorrow() -> Date {
        let now = Local::today().succ();
        Date::Day(now.year(), now.month(), now.day())
    }

    pub fn yesterday() -> Date {
        let now = Local::today().pred();
        Date::Day(now.year(), now.month(), now.day())
    }

    pub fn next_weekday(wd: Weekday) -> Date {
        let mut now = Local::today().succ();
        while now.weekday() != wd {
            now = now.succ();
        }
        Date::Day(now.year(), now.month(), now.day())
    }

    pub fn from(text: &str) -> Option<Date> {
        if let Some(date) = Date::parse_day(text) {
            return Some(date);
        }

        if let Some(date) = Date::parse_month(text) {
            return Some(date);
        }

        // special cases
        match text {
            "today" => Some(Date::today()),
            "tomorrow" => Some(Date::tomorrow()),
            "yesterday" => Some(Date::yesterday()),
            "mon" => Some(Date::next_weekday(Weekday::Mon)),
            "tue" => Some(Date::next_weekday(Weekday::Tue)),
            "wed" => Some(Date::next_weekday(Weekday::Wed)),
            "thu" => Some(Date::next_weekday(Weekday::Thu)),
            "fri" => Some(Date::next_weekday(Weekday::Fri)),
            "sat" => Some(Date::next_weekday(Weekday::Sat)),
            "sun" => Some(Date::next_weekday(Weekday::Sun)),
            _ => None
        }
    }

    fn parse_day(text: &str) -> Option<Date> {
        lazy_static! {
            static ref DAY_RE: Regex = Regex::new(r"^(\d{4})-(\d{2})-(\d{2})$").unwrap();
        }
        match DAY_RE.captures(text) {
            Some(caps) => Some(Date::Day(
                caps[1].parse().unwrap(),
                caps[2].parse().unwrap(),
                caps[3].parse().unwrap()
            )),
            None => None
        }
    }

    fn parse_month(text: &str) -> Option<Date> {
        lazy_static! {
            static ref MONTH_RE: Regex = Regex::new(r"^(\d{4})-(\d{2})$").unwrap();
        }
        match MONTH_RE.captures(text) {
            Some(caps) => Some(Date::Month(
                caps[1].parse().unwrap(),
                caps[2].parse().unwrap()
            )),
            None => None
        }
    }

    pub fn weekday(&self) -> Option<Weekday> {
        match self {
            Date::Day(y, m, d) => {
                if let LocalResult::Single(date) = Local.ymd_opt(*y, *m, *d) {
                    Some(date.weekday())
                } else {
                    None
                }
            },
            Date::Month(_, _) => None
        }
    }
}

impl Ord for Date {
    fn cmp(&self, other: &Self) -> Ordering {
        match other {
            Date::Day(yy, mm, dd) =>
            match self {
                Date::Day(y, m, d) => {
                    let x = y.cmp(yy);
                    if x != Ordering::Equal { return x; }

                    let x = m.cmp(mm);
                    if x != Ordering::Equal { return x; }

                    d.cmp(dd)
                }
                Date::Month(y, m) => {
                    let x = y.cmp(yy);
                    if x != Ordering::Equal { return x; }

                    let x = m.cmp(mm);
                    if x != Ordering::Equal { return x; }

                    Ordering::Less
                }
            }
            Date::Month(yy, mm) =>
            match self {
                Date::Month(y, m) => {
                    let x = y.cmp(yy);
                    if x != Ordering::Equal { return x; }
                    m.cmp(mm)
                }
                Date::Day(y, m, _d) => {
                    let x = y.cmp(yy);
                    if x != Ordering::Equal { return x; }

                    let x = m.cmp(mm);
                    if x != Ordering::Equal { return x; }

                    Ordering::Greater
                }
            }
        }
    }
}

impl PartialOrd for Date {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

impl PartialEq for Date {
    fn eq(&self, other: &Self) -> bool {
        match self {
            Date::Day(y, m, d) =>
                if let Date::Day(yy, mm, dd) = other { y == yy && m == mm && d == dd } else { false }
            Date::Month(y, m) =>
                if let Date::Month(yy, mm) = other { y == yy && m == mm } else { false }
        }
    }
}

impl fmt::Display for Date {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Date::Day(y, m, d) => write!(f, "{:04}-{:02}-{:02}", y, m, d),
            Date::Month(y, m) => write!(f, "{:04}-{:02}", y, m)
        }
    }
}

#[derive(Eq, Debug)]
pub struct Task {
    pub done: bool,
    pub due: Date,
    pub priority: i32,
    pub text: String
}

impl Task {
    pub fn from(text: &str) -> Option<Task> {
        let mut tokens: Vec<&str> = text.split(' ').collect();
        let done = tokens[0] == "x";
        if done {
            tokens.remove(0);
        }

        let due: Date;
        if let Some(date) = Date::from(tokens[0]) {
            due = date;
            tokens.remove(0);
        } else {
            return None;
        }

        let priority: i32;
        if !done {
            if let Ok(pri) = tokens[0].parse::<i32>() {
                priority = pri;
                tokens.remove(0);
            } else {
                return None;
            }
        } else {
            priority = 0;
        }

        Some(Task {
            done: done,
            due: due,
            priority: priority,
            text: tokens.join(" ")
        })
    }
}

impl fmt::Display for Task {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        if self.done {
            write!(f, "x {} {}", self.due, self.text)
        } else {
            write!(f, "{} {} {}", self.due, self.priority, self.text)
        }
    }
}

impl Ord for Task {
    fn cmp(&self, other: &Self) -> Ordering {
        if self.due != other.due { self.due.cmp(&other.due) }
        else if self.done && !other.done { Ordering::Less }
        else if !self.done && other.done { Ordering::Greater }
        else { self.priority.cmp(&other.priority) }
    }
}

impl PartialOrd for Task {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

impl PartialEq for Task {
    fn eq(&self, other: &Self) -> bool {
        self.done == other.done
            && self.due == other.due
            && self.priority == other.priority
            && self.text == other.text
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn date_eq() {
        assert_eq!(Date::Day(2021, 3, 12), Date::Day(2021, 3, 12));
        assert_eq!(Date::Month(2021, 8), Date::Month(2021, 8));
        assert_ne!(Date::Day(2021, 3, 12), Date::Day(2021, 8, 6));
        assert_ne!(Date::Month(2021, 3), Date::Month(2021, 8));
        assert_ne!(Date::Day(2021, 3, 12), Date::Month(2021, 3));
    }

    #[test]
    fn date_ord() {
        assert!(Date::Day(2021, 3, 12) < Date::Day(2021, 3, 13));
        assert!(Date::Day(2021, 3, 12) < Date::Day(2021, 8, 6));
        assert!(Date::Day(2020, 3, 12) < Date::Day(2021, 8, 6));
        assert!(Date::Day(2020, 3, 12) < Date::Month(2021, 3));
    }
}
