#[macro_use]
extern crate lazy_static;

pub mod types;
pub mod parse;
use types::{Date, Task};

use std::fs;
use std::cmp;

// adds the task and recalculates priorities
fn add_task(tasks: &mut Vec<Task>, due: Date, done: bool, text: String, top: bool) {
    let today_tasks: Vec<&mut Task> = tasks.iter_mut().filter(|x| x.due == due && !x.done).collect();
    let priority = if done { 0 } else if top {
        for x in today_tasks { x.priority += 1 }
        0
    } else {
        let max_pri = today_tasks.iter().fold(-1, |acc, x| cmp::max(acc, x.priority));
        max_pri + 1
    };
    tasks.push(Task {
        due: due,
        priority: priority,
        done: done,
        text: text
    });
}

// removes the task and sorts priorities of the day
fn remove_task(tasks: &mut Vec<Task>, due: Date, pri: i32, done: bool) -> Option<Task> {
    let pos = find_task(tasks, due, pri, done);
    if pos == None {
        return None;
    }
    let pos = pos.unwrap();

    tasks.iter_mut().for_each(|x| if x.due == due && x.priority > pri { x.priority -= 1 });
    Some(tasks.remove(pos))
}

pub fn find_task(tasks: &Vec<Task>, due: Date, pri: i32, done: bool) -> Option<usize> {
    tasks.iter().position(|x| x.due == due && x.priority == pri && x.done == done)
}

fn read_file(fname: &str) -> Vec<String> {
    let blob = fs::read_to_string(fname).expect(&format!("Couldn't read a file: '{}'.", fname)[..]);
    blob.split_terminator('\n').map(|x| String::from(x)).collect()
}

// TODO verify integrity (correct dates, unique priorities)
pub fn read_tasks(fname: &str) -> Vec<Task> {
    let lines = read_file(fname);
    let mut tasks: Vec<Task> = lines.into_iter()
        .map(|x| Task::from(&x[..]))
        .filter(|x| if let None = x { false } else { true })
        .map(|x| x.unwrap())
        .collect();
    tasks.sort_unstable();
    //tasks.reverse();
    let tasks = tasks;
    tasks
}

pub fn write_tasks(todo_file: &str, tasks: &mut Vec<Task>) {
        tasks.sort_unstable();
        // TODO write to the end only
        fs::write(todo_file, tasks.iter()
            .map(|x| format!("{}\n", x))
            .collect::<String>())
            .expect("unable to write to a file");
}

pub fn add(tasks: &mut Vec<Task>, due: Option<String>, done: bool, text: String, top: bool) -> Result<(), &str> {
    let due = due.unwrap_or(String::from("today"));
    let due = Date::from(&due[..]);

    if due == None {
        return Err("due format is YYYY-MM[-DD]");
    }

    add_task(tasks, due.unwrap(), done, text, top);
    Ok(())
}

pub fn rm(tasks: &mut Vec<Task>, due: Option<String>, pri: Option<String>) -> Result<(), &str> {
    let due = due.unwrap_or(String::from("today"));
    let due = Date::from(&due[..]);
    if due == None {
        return Err("date format is either YYYY-MM or YYYY-MM-DD");
    }

    let pri = pri.unwrap_or(String::from("0"));
    let pri = str::parse::<i32>(&pri[..]);
    if pri.is_err() {
        return Err("priority must be a number");
    }

    if None == remove_task(tasks, due.unwrap(), pri.unwrap(), false) {
        return Err("the requested task is not found");
    }
    Ok(())
}

pub fn done(tasks: &mut Vec<Task>, due: Option<String>, pri: Option<String>) -> Result<(), &str> {
    let due = due.unwrap_or(String::from("today"));
    let due = Date::from(&due[..]);
    if due == None {
        return Err("date format is either YYYY-MM or YYYY-MM-DD");
    }
    let due = due.unwrap();

    let pri = pri.unwrap_or(String::from("0"));
    let pri = str::parse::<i32>(&pri[..]);
    if pri.is_err() {
        return Err("priority must be a number");
    }
    let pri = pri.unwrap();

    let pos = find_task(tasks, due, pri, false);
    if pos == None {
        return Err("the requested task is not found");
    }
    let pos = pos.unwrap();

    tasks[pos].done = true;
    let today = Date::today();
    if tasks[pos].due > today { tasks[pos].due = today; }
    let pri = tasks[pos].priority;
    tasks.iter_mut().for_each(|x| if x.due == due && x.priority > pri { x.priority -= 1 });
    Ok(())
}

pub fn mv<'a>(tasks: &'a mut Vec<Task>, from: Option<String>, to: Option<String>, at: Option<&str>, pri: Option<String>, top: bool) -> Result<(), &'a str> {
    let at = at.unwrap_or("today");

    let from = from.unwrap_or(String::from(at));
    let from = Date::from(&from[..]);
    if from == None {
        return Err("date format is either YYYY-MM or YYYY-MM-DD");
    }
    let from = from.unwrap();

    let to = to.unwrap_or(String::from(at));
    let to = Date::from(&to[..]);
    if to == None {
        return Err("date format is either YYYY-MM or YYYY-MM-DD");
    }
    let to = to.unwrap();

    let pri = pri.unwrap_or(String::from("0"));
    let pri = str::parse::<i32>(&pri[..]);
    if pri.is_err() {
        return Err("priority must be a number");
    }
    let pri = pri.unwrap();

    let task = remove_task(tasks, from, pri, false);
    if task == None {
        return Err("the requested task is not found");
    }
    let task = task.unwrap();
    add_task(tasks, to, task.done, task.text, top);
    Ok(())
}

pub fn swap(tasks: &mut Vec<Task>, pri1: Option<String>, pri2: Option<String>, at: Option<String>) -> Result<(), &str> {
    let at = at.unwrap_or(String::from("today"));
    let at = Date::from(&at[..]);
    if at == None {
        return Err("date format is either YYYY-MM or YYYY-MM-DD");
    }
    let at = at.unwrap();

    if pri1 == None {
        return Err("priority must be set");
    }
    let pri1 = pri1.unwrap();
    let pri1 = str::parse::<i32>(&pri1[..]);
    if pri1.is_err() {
        return Err("priority must be a number");
    }
    let pri1 = pri1.unwrap();

    if pri2 == None {
        return Err("the second priority must be set");
    }
    let pri2 = pri2.unwrap();
    let pri2 = str::parse::<i32>(&pri2[..]);
    if pri2.is_err() {
        return Err("the second priority must be a number");
    }
    let pri2 = pri2.unwrap();

    let pos1 = find_task(tasks, at, pri1, false);
    if pos1 == None {
        return Err("the first task not found");
    }
    let pos1 = pos1.unwrap();
    let pos2 = find_task(tasks, at, pri2, false);
    if pos2 == None {
        return Err("the second task not found");
    }
    let pos2 = pos2.unwrap();
    tasks[pos1].priority = pri2;
    tasks[pos2].priority = pri1;
    Ok(())
}

// TODO add a different function for a string construction
// TODO add 'at' parameter
pub fn print_list(tasks: &Vec<Task>, at: Option<String>, done: bool, all: bool) -> Result<(), &str> {
    let at = at.unwrap_or(String::from("today"));
    let at = Date::from(&at[..]);
    if at == None {
        return Err("date format is either YYYY-MM or YYYY-MM-DD");
    }
    let at = at.unwrap();
    let today = Date::today();

    let mut due = Date::Month(0, 0);
    let mut empty = true;
    for task in tasks.iter() {
        if task.done && task.due < at { continue }
        if task.done && !done { continue }
        if task.due != at && !all { continue }
        if due != task.due {
            due = task.due;
            println!("\n{}{}{}:",
                due.weekday().map(|x| format!("{}, ", x.to_string())).unwrap_or(String::from("")),
                due, if all && task.due == today { " (today)" } else { "" });
        }
        if task.done {
            println!("x. {}", task.text);
        } else {
            println!("{}. {}", task.priority, task.text);
        }
        if empty {
            empty = !empty;
        }
    }
    if empty {
        // TODO print a different message when all the tasks are done
        println!("No tasks spotted!");
    }
    Ok(())
}

fn print_progress(n: usize, max: usize) {
    println!("[{}{}] {}/{}", "|".repeat(n), " ".repeat(max - n), n, max)
}

pub fn show_current(tasks: &Vec<Task>) {
    let today = Date::today();
    let tasks: Vec<&Task> = tasks.iter().filter(|x| x.due == today).collect();

    let max = tasks.len();
    let mut n = 0;

    let mut current: Option<&Task> = None;
    for task in tasks {
        if !task.done { current = Some(task); break; }
        n += 1;
    }

    print_progress(n, max);
    if let Some(t) = current {
        println!("\n{}", t.text);
    } else {
        println!("\nWhoo! No tasks left!");
    }
}
